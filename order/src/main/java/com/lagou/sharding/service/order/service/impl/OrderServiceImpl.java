package com.lagou.sharding.service.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.business.service.OrderService;
import com.lagou.sharding.common.plugins.jwt.JwtTokenService;
import com.lagou.sharding.common.pojo.ResultInfo;
import com.lagou.sharding.common.utils.RedisUtil;
import com.lagou.sharding.service.order.dao.OrderDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderDao, Order> implements OrderService {

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private OrderDao orderDao;


    @Override
    public ResultInfo<Boolean> saveOrder(Order order) {
        if(this.saveOrUpdate(order)){
            return ResultInfo.success(true);
        }
        return ResultInfo.error("保存失败！");
    }

    @Override
    public ResultInfo<Order> oneOder(Long orderId) {
        return ResultInfo.success(this.getById(orderId));
    }

    @Override
    public ResultInfo<List<Order>> findByPubUserId(Integer publishUserId) {
        List<Order> orders = orderDao.selectList(new QueryWrapper<Order>().lambda().eq(Order::getPublishUserId, publishUserId));
        return ResultInfo.success(orders);
    }
}
