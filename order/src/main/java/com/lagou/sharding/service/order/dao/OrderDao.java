package com.lagou.sharding.service.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lagou.sharding.business.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDao extends BaseMapper<Order> {
}
