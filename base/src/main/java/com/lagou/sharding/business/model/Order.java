package com.lagou.sharding.business.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_order")
public class Order extends Model<Order> {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    @TableField
    private Boolean isDel = false;
    @TableField
    private Integer userId = 0;
    @TableField
    private Integer companyId = 0;
    @TableField
    private Integer publishUserId = 0;
    @TableField
    private Integer positionId = 0;
    //简历类型：0附件 1在线
    @TableField
    private Integer resumeType = 1;
    //投递状态 WAIT-待处理理 AUTO_FILTER-自动过滤 PREPARE_CONTACT-待沟通 REFUSE-拒绝 ARRANGE_INTERVIEW-通知面试
    @TableField
    private String status = "WAIT";
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;



}
