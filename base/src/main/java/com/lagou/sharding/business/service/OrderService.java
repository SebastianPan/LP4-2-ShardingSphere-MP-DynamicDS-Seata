package com.lagou.sharding.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.common.pojo.ResultInfo;

import java.util.List;

public interface OrderService extends IService<Order> {

    ResultInfo<Boolean> saveOrder(Order order);

    ResultInfo<Order> oneOder(Long orderId);

    ResultInfo<List<Order>> findByPubUserId(Integer publishUserId);
}
